var BaseCollection = Backbone.Collection;

var ProjectModel = require('models/project-model');

var CredentialsCollection = require('collections/credentials-collection');

module.exports = BaseCollection.extend({

    model: ProjectModel,

    url: '/projects',

    parse: function (json) {
        _.each(json, function (project) {
            project.credentials = new CredentialsCollection(project.credentials);
        });

        return json;
    }
})