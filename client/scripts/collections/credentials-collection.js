'use strict';

var BaseCollection = Backbone.Collection;
var CredentialModel = require('models/credential-model');

module.exports = BaseCollection.extend({
    model: CredentialModel
})