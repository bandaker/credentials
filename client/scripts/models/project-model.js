'use strict';

var BaseModel = require('base-model');

var CredentialsCollection = require('collections/credentials-collection');

module.exports = BaseModel.extend({

    defaults: function () {
        return {
            credentials: new CredentialsCollection()
        };
    },

    type: 'project'
});
