'use strict';

var BaseModel = require('base-model');

module.exports = BaseModel.extend({
    type: 'credential'
});
