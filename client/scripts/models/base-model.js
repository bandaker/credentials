'use strict';

var __base__ = Backbone.Model;
var __super__ = __base__.prototype;

module.exports = __base__.extend({
    url: function () {
        return '/save?_id=' + this.get('_id') + '&type=' + this.type;
    },

    isNew: function () {
        return !this.get('_id');
    },

    toJSON: function () {
        return _.extend(__super__.toJSON.apply(this, arguments), { notNew: !this.isNew() });
    },

    idAttribute: '_id'
});
