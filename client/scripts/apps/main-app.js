'use strict';

var ProjectsCollection = require('collections/projects-collection');
var ProjectsView = require('views/projects-view');

require('lib/handlebars-helpers');

function App() {
}

App.prototype = _.extend(App.prototype, {
    initialize: function () {
        var projectsCollection = new ProjectsCollection();
        projectsCollection.fetch({
            success: function () {
                var view = new ProjectsView({
                    collection: projectsCollection
                });

                view.render();
            }
        });
    }
});

module.exports = App;
