'use strict';

var BaseView = require('base-view');

module.exports = BaseView.extend({
    events: {
        'click .save': 'save',
        'click .edit': 'toggleMode',
        'click .delete': 'deleteModel'
    },

    deleteModel: function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (!window.confirm('Are you sure you would like to delete?')) {
            return;
        }
        this.model.destroy();
        this.remove();
    },

    toggleMode: function (e) {
        if (e) {
            e.preventDefault();
            e.stopPropagation();
        }
        this.$('form').find('.display').toggleClass('hide');
        this.$('form').find('input').toggleClass('hide');

        this.$('.save').toggleClass('hide');
    },

    renderForm: function () {
        this.$('form').html(this.formTemplate(this.model.toJSON()));
    },

    save: function (e) {
        e.preventDefault();
        e.stopPropagation();
        var data = $(e.currentTarget).closest('form').serializeJSON();
        this.model.save(data);
        this.renderForm();
    }
});