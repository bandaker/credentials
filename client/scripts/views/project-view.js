'use strict';
var BaseView = require('./editable-view');

var Handlebars = require('handlebars');
var templates = require('templates')(Handlebars);

var CredentialModel = require('models/credential-model');

var CredentialsCollection = require('collections/credentials-collection');

var CredentialView = require('./credential-view');

module.exports = BaseView.extend({
    template: templates['project'],

    formTemplate: templates['project-form'],

    events: _.extend(BaseView.prototype.events, {
        'click .add-credential': 'addCredential'
    }, {}),

    credentialViews: [],

    className: 'row project',

    addCredential: function () {
        var newCredentialModel = new CredentialModel({
            project: this.model.get('_id')
        });

        var newCredentialView = new CredentialView({
            model: newCredentialModel
        });

        this.$('.credentials').prepend(newCredentialView.render().$el);

        this.model.get('credentials').add(newCredentialModel);

        this.credentialViews.push(newCredentialView);
    },

    render: function () {
        BaseView.prototype.render.apply(this, arguments);

        this.renderForm();
        if (this.model.isNew()) {
            this.toggleMode();
            var that = this;
            this.model.on('change:_id', function () {
                that.model.set('credentials', new CredentialsCollection());
                that.render();
            });
        }

        var credentialView;
        _.each(this.model.get('credentials').models, function (model) {
            credentialView = new CredentialView({
                model: model
            });
            this.$('.credentials').prepend(credentialView.render().$el);
            this.credentialViews.push(credentialView);
        }, this);

        return this;
    },

    remove: function () {
        _.each(this.model.get('credentials').models, function (model) {
            model.destroy();
        });

        _.each(this.credentialViews, function (view) {
            view.remove();
        });

        BaseView.prototype.remove.apply(this, arguments);
    }
});
