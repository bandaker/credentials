'use strict';

var __super__ = Backbone.View.prototype;
var handlebars = require('handlebars');
var templates = require('templates')(handlebars);

module.exports = Backbone.View.extend({

    // define template to be render automatically
    template: undefined,
    className: 'row',

    render: function () {
        var data = this.model ? this.model.toJSON() : {};
        this.$el.html(this.template(data));
        return this;
    },

    afterRender: function () {}

});
