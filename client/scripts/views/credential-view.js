'use strict';
var BaseView = require('./editable-view');

var Handlebars = require('handlebars');
var templates = require('templates')(Handlebars);

module.exports = BaseView.extend({

    template: templates['credential'],

    formTemplate: templates['credential-form'],

    render: function () {

        BaseView.prototype.render.apply(this, arguments);
        var data = this.model.toJSON();
        data.notNew = !this.model.isNew();
        this.$('form').html(this.formTemplate(data));

        if (this.model.isNew()) {
            this.toggleMode();
            this.model.on('change:_id', this.render.bind(this));
        }

        return this;
    }
});
