'use strict';
var BaseView = require('base-view');

var Handlebars = require('handlebars');
var templates = require('templates')(Handlebars);

var ProjectView = require('views/project-view');

var ProjectModel = require('models/project-model');

module.exports = BaseView.extend({
    template: templates['projects'],

    el: '#content',

    events: {
        'click .add': 'addProject'
    },

    addProject: function () {
        var newProjectModel = new ProjectModel();

        var newProjectView = new ProjectView({
            model: newProjectModel
        });

        this.$('.projects').prepend(newProjectView.render().$el);

        this.projectViews.push(newProjectView);
    },

    projectViews: [],

    render: function () {
        BaseView.prototype.render.apply(this, arguments);

        var projectView;
        _.each(this.collection.models, function (model) {
            projectView = new ProjectView({
                model: model
            });
            this.$('.projects').prepend(projectView.render().$el);
            this.projectViews.push(projectView);
        }, this);
    }
});
