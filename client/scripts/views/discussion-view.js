'use strict';
var BaseView = require('base-view');

var Handlebars = require('handlebars');
var templates = require('templates')(Handlebars);

CKEDITOR.config.toolbar = [
   ['Styles','Format','Font','FontSize'],
   '/',
   ['Bold','Italic','Underline','StrikeThrough','-','Undo','Redo','-','Cut','Copy','Paste','Find','Replace','-','Outdent','Indent','-','Print'],
   '/',
   ['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock']
];

module.exports = BaseView.extend({
    template: templates['discussion'],

    responseTemplate: templates['util/response'],

    events: {
        'click button': 'getReply'
    },

    getReply: function () {
        var response = CKEDITOR.instances[this.model.get('topictitle').replace(/ /g,'')].getData();
        CKEDITOR.instances[this.model.get('topictitle').replace(/ /g,'')].setData('');

        var data = {
            posttext: response,
            age: 0
        };
        this.model.get('responses').push(data);

        this.renderResponse(data);
    },

    render: function (argument) {
        this.$el.html(this.template({
            title: this.model.get('topictitle'),
            unique: this.model.get('topictitle').replace(/ /g,'')
        }));

        // this.prepEditor();

        _.each(this.model.get('responses'), function (response) {
            this.renderResponse(response);
        }, this);

        return this;
    },

    prepEditor: function () {
        CKEDITOR.replace(this.model.get('topictitle').replace(/ /g,''));
    },

    renderResponse: function (response) {
        this.$('.responses').append(this.responseTemplate(response));
    }
});
