'use strict';

var Handlebars = require('handlebars');
var templates = require('templates')(Handlebars);

Handlebars.registerHelper({

    editable: function (name, value) {
        return new Handlebars.SafeString(templates['util/editable']({
            name: name,
            value: value
        }));
    }

});

module.exports = Handlebars;
