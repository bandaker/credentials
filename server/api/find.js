require('./db');

module.exports = function (id, type, callBack) {
     var query = typeMap[type].find({ '_id': id });

      query.exec(function (err, result) {
        if (err) {
            res.send('error');
        } else if (result.length) {
            callBack(result[0]);
        } else {
            callBack(false);
        }
      });
};