'use strict';

require('./db');

module.exports = function (req, res) {
    ProjectRecord.find({})
        .populate('credentials')
        .exec(function (err, projects) {
            if (err) {
                res.send(false);
            } else {
                res.send(projects)
            }
        });
}