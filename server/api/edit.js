'use strict';

require('./db');

var _ = require('lodash');

var find = require('./find');

module.exports = function (req, res) {

    var id = req.param('_id');

    var data = req.body;

    var found = function (item) {
        if (!item) {
            res.send(false)
        } else {
            item = _.extend(item, data);

            item.save(function (err, succes) {
                    if (err) {
                        res.send(err);
                    } else {
                        res.send(item);
                    }
            });
        }
    }

    find(id, req.param('type'), found);
};

