'use strict';

require('./db');

module.exports = function (data, type, callback) {
    var Record = typeMap[type];

    var record = new Record(data);

    record.save(function (err, success) {
        if (err) {
            callback(err);
        } else {
            callback(record);
        }
    });
};


// var parent = new Parent({});
// parent.save();
// var child = new Child({parent:parent._id});
// child.save() //the parent children property will now contain child's id
// child.remove()