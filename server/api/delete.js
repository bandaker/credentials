'use strict';

require('./db');

var _ = require('lodash');

var find = require('./find');

module.exports = function (req, res) {

    var id = req.param('_id');

    var type = req.param('type');

    var found = function (item) {
        if (!item) {
            res.send(false)
        } else {
            item.remove(function (err, succes) {
                if (err) {
                    res.send(err);
                } else {
                    res.send(item);
                }
            });
        }
    }

    find(id, type, found);
};
