var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var relationship = require('mongoose-relationship');

var mongoUri = process.env.MONGOLAB_URI ||
  process.env.MONGOHQ_URL ||
  'mongodb://localhost/test';

mongoose.connect(mongoUri);

ProjectSchema = new Schema({
    credentials:[{ type:Schema.ObjectId, ref:'Credential' }],
    name: String,
    description: String
});

ProjectRecord = mongoose.model('Project', ProjectSchema);

var CredentialSchema = new Schema({
    project: { type:Schema.ObjectId, ref:'Project', childPath:'credentials' },
    username: String,
    password: String,
    host: String,
    port: Number
});

CredentialSchema.plugin(relationship, { relationshipPathName:'project' });
CredentialRecord = mongoose.model('Credential', CredentialSchema);

typeMap = {
  'project': ProjectRecord,
  'credential': CredentialRecord
}
