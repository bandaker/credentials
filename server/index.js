'use strict';

var express = require('express');
var app = express();
app.use(express.bodyParser());

//fetch
app.get('/projects', require('./api/fetch'));

//save
var save = require('./api/save');
app.post('/save', function (req, res) {
    save(req.body, req.param('type'), function (item) {
        res.send(item)
    });
});

//update
var edit = require('./api/edit');
app.put('/save', edit);

//delete
var deleteItem = require('./api/delete');
app.delete('/save', deleteItem);

//app root
app.get('/', function (req, res) {
    res.sendfile(__dirname + '/web/index.html');
});

//assets
app.use('/web',express.static(__dirname + '/web'));

var port = Number(process.env.PORT || 5000);
console.log('port', port);
app.listen(port, function () {
    console.log('Listening on ' + port);
});