
var mongoose = require('mongoose');

var should = require('should');
var _ = require('lodash');
function projectData() {
    return {
        name: 'test project',
        description: 'this is a test',
        type: 'project'
    };
}

function credentialData() {
    return {
        username: 'u',
        password: 'p',
        type: 'credential'
    };
};

var args = {
    data: projectData(),
    headers:{ 'Content-Type': 'application/json' }
};

var NodeRestClient = require('node-rest-client').Client;
client = new NodeRestClient();

var baseUrl = 'http://localhost:3002/';

process.env.environment = 'test';
process.env.PORT = 3002;
require('../server/index');

describe('Models', function () {
    afterEach(function (done) {
        ProjectRecord.find({}).remove({}, function () {
            CredentialRecord.find({}).remove({}, function () {
                done();
            });
        });
    });

    it('can send post to create a project', function (done) {

        client.post(baseUrl + 'save', args, function (data,response) {
            data =JSON.parse(data);
            data['name'].should.equal(projectData().name);
            ProjectRecord.find({ '_id': data['_id'] }, function (err, doc) {
                doc[0].name.should.equal(projectData().name);
                doc[0].description.should.equal(projectData().description);
                done();
            })
        });

    });

    it('can send patch to update a project', function (done) {

        client.post(baseUrl + 'save', args, function (data,response) {
            data = JSON.parse(data);
            ProjectRecord.find({ '_id': data['_id'] }, function (err, doc) {

                var newName = 'abc';

                var data = {
                    '_id': doc[0]['_id'],
                    name: newName,
                    type: 'project'
                };

                args.data = data;
                client.put(baseUrl + 'save', args, function (data, response) {
                    data = JSON.parse(data);
                    data['name'].should.equal(newName);
                    ProjectRecord.find({ '_id': data['_id'] }, function (err, doc) {
                        doc[0].name.should.equal(newName);
                        done();

                    });
                });
            });
        });

    });


    it('can send post to create a credential', function (done) {

        client.post(baseUrl + 'save', args, function (data,response) {
            data = JSON.parse(data);
            args.data = credentialData();
            args.data.project = data['_id'];
            client.post(baseUrl + 'save', args, function (data,response) {
                data = JSON.parse(data);
                data['username'].should.equal(credentialData().username);
                done();
            });
        });

    });

    it('can send patch to update a credential', function (done) {

        client.post(baseUrl + 'save', args, function (data,response) {
            data = JSON.parse(data);
            args.data = credentialData();
            args.data.project = data['_id'];

            client.post(baseUrl + 'save', args, function (data,response) {
                data = JSON.parse(data);
                var newName = 'abc';

                var data = {
                    '_id': data['_id'],
                    username: newName,
                    type: 'credential'
                };

                args.data = data;
                client.put(baseUrl + 'save', args, function (data, response) {
                    data = JSON.parse(data);
                    data['username'].should.equal(newName);

                    CredentialRecord.find({ '_id': data['_id'] }, function (err, doc) {
                        doc[0].username.should.equal(newName);
                        done();
                    });
                });
            });
        });

    });

    it('can fetch projects and credentials', function (done) {

        args.data = projectData();

        client.post(baseUrl + 'save', args, function (data,response) {
            data = JSON.parse(data);
            args.data = credentialData();
            args.data.project = data['_id'];

            client.post(baseUrl + 'save', args, function (data,response) {
                client.get(baseUrl + 'projects', {}, function (data) {
                    data = JSON.parse(data);
                    data[0].credentials[0].username.should.equal(credentialData().username)
                    data[0].name.should.equal(projectData().name)
                    done();
                });
            });
        });
    });


    it('can delete projects', function (done) {

        args.data = projectData();

        client.post(baseUrl + 'save', args, function (data,response) {
            data = JSON.parse(data);
            // args.data = credentialData();
            args.data['_id'] = data['_id'];

            client.delete(baseUrl + 'save', args, function (data,response) {
                ProjectRecord.find({}, function (err, doc) {
                    doc.length.should.equal(0);
                    done();
                });
            });
        });
    });

    it('can delete credential', function (done) {

        args.data = credentialData();

        client.post(baseUrl + 'save', args, function (data,response) {
            data = JSON.parse(data);
            // args.data = credentialData();
            args.data['_id'] = data['_id'];

            client.delete(baseUrl + 'save', args, function (data,response) {
                CredentialRecord.find({}, function (err, doc) {
                    doc.length.should.equal(0);
                    done();
                });
            });
        });
    });

});