'use strict';
module.exports = function (grunt) {

grunt.loadNpmTasks('grunt-mocha-test');

  grunt.config.set('mochaTest',{
        options: {
          reporter: 'spec',
          // Require blanket wrapper here to instrument other required
          // files on the fly.
          //
          // NB. We cannot require blanket directly as it
          // detects that we are not running mocha cli and loads differently.
          //
          // NNB. As mocha is 'clever' enough to only run the tests once for
          // each file the following coverage task does not actually run any
          // tests which is why the coverage instrumentation has to be done here
        },
        src: ['tests/**/*.js']
  });

  grunt.registerTask('default', 'mochaTest');

  return grunt;
};

